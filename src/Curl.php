<?php
/**
 * Curl class
 *
 * @package Liberty
 * @author Liberty Concepts <info@libertyconcepts.com>
 * @copyright Copyright (c) 2014, Liberty Concepts (https://libertyconcepts.com)
 * @license http://opensource.org/licenses/OSL-3.0 Open Software License 3.0
 * @version 0.1.0
 */

namespace Liberty;

use PDO;
use Exception;
use PDOException;

class Curl
{
    private $options = [];
    private $data = [];
    private $verbose = true;

    public function __construct()
    {
        $this->handle = curl_init();
        if (php_sapi_name() === 'cli') {
            $this->verbose = false;
        }
    }

    public function __destruct()
    {
        $this->closeHandle();
    }

    public function closeHandle()
    {
        curl_close($this->handle);
    }

    public function getInfo()
    {
        return (object) $this->return_info;
    }

    public function get($url)
    {
        curl_setopt($this->handle, CURLOPT_URL, $url);
        curl_setopt_array($this->handle, $this->options);
        curl_setopt($this->handle, CURLOPT_VERBOSE, $this->verbose);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($this->handle);
        $info = curl_getinfo($this->handle);
        $this->return_info = $info;
        return $output;
    }

    public function post($url)
    {
        curl_setopt($this->handle, CURLOPT_URL, $url);
        curl_setopt_array($this->handle, $this->options);
        curl_setopt($this->handle, CURLOPT_POST, true);
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, $this->data);
        curl_setopt($this->handle, CURLOPT_VERBOSE, $this->verbose);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $this->handle,
            CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: '.strlen($this->data)
            ]
        );
        $output = curl_exec($this->handle);
        $info = curl_getinfo($this->handle);
        $this->return_info = $info;
        return $output;
    }

    public function setData($data = [])
    {
        $this->data = json_encode($data);
    }

    public function setOptions($options = [])
    {
        $this->options = $options;
    }

    private function open()
    {
        $ch = curl_init();
        return $ch;
    }
}
